﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Examen1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .text {
          font-size:28px;
          font-family:helvetica;
          font-weight:bold;
          color:#71d90b;
          text-transform:uppercase;
        }
        .parpadea {
  
          animation-name: parpadeo;
          animation-duration: 1s;
          animation-timing-function: linear;
          animation-iteration-count: infinite;

          -webkit-animation-name:parpadeo;
          -webkit-animation-duration: 1s;
          -webkit-animation-timing-function: linear;
          -webkit-animation-iteration-count: infinite;
        }

        @-moz-keyframes parpadeo{  
          0% { opacity: 1.0; }
          50% { opacity: 0.0; }
          100% { opacity: 1.0; }
        }

        @-webkit-keyframes parpadeo {  
          0% { opacity: 1.0; }
          50% { opacity: 0.0; }
           100% { opacity: 1.0; }
        }

        @keyframes parpadeo {  
          0% { opacity: 1.0; }
           50% { opacity: 0.0; }
          100% { opacity: 1.0; }
        }
    </style>
    <div class="jumbotron">
        <h3>Instrucciones: Examen HTML</h3>
        <hr />
        <p class="lead">Crear el HTML mostrado cumpliendo con los siguientes parámetros:</p>
        <ul>
            <li>(OK) El nombre debe tener al menos UNA (1) letra Mayúscula.</li>
            <li>(OK) La edad SOLAMENTE pueden ser números.</li>
            <li>(OK) Mostrar una lista con 5 diferentes opciones en el campo Occupation.</li>
            <li>(OK) El password debe ser entre 4 y 8 caracteres y debe tener un número.</li>
            <li>(OK) El botón Submit debe guardar la información en una tabla de base de datos (puede ser ficticia) y Reset debe vaciar los campos. Utilizar ASP.NET para este paso.</li>
            <li>(OK) Los 6 campos externos deben mostrar un mensaje intermitente (opcional).</li>
        </ul>
    </div>
    <div class="row">
        <table class="table">
            <tr>
                <td colspan="2">
                    <span class="parpadea text">TopManage</span>
                </td>
                <td colspan="2">
                    <span class="parpadea text">Examen 1</span>
                </td>
                <td colspan="2">
                    <span class="parpadea text">HTML</span>
                </td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <td>Name</td>
                <td colspan="4">
                    <asp:TextBox ID="txt_name" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Age</td>
                <td colspan="2">
                    <asp:TextBox ID="txt_age" runat="server" CssClass="form-control" OnTextChanged="txt_age_TextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
                <td>Years</td>
                <td>
                    <asp:Label ID="lbl_years" runat="server" CssClass="form-control"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Occupation</td>
                <td colspan="4">
                    <asp:DropDownList ID="select_occupation" runat="server" CssClass="form-control">
                        <asp:ListItem Value ="-1" Text="Select an option"></asp:ListItem>

                        <asp:ListItem Value="Baker" Text="Baker"></asp:ListItem>
                        <asp:ListItem Value="Carpenter" Text="Carpenter"></asp:ListItem>
                        <asp:ListItem Value="Plumber" Text="Plumber"></asp:ListItem>
                        <asp:ListItem Value="Craftsman" Text="Craftsman"></asp:ListItem>
                        <asp:ListItem Value="Blacksmith" Text="Blacksmith"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td colspan="4">
                    <asp:TextBox ID="txt_pwd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:Label ID="lbl_message" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="commit_data" runat="server" Text="Submit" CssClass="btn btn-primary-lg btn-success" OnClick="commit_data_Click" />
                </td>
                <td>
                    <asp:Button ID="reset_data" runat="server" Text="Reset" CssClass="btn btn-primary-lg btn-warning" OnClick="reset_data_Click"/>
                </td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <td colspan="2">
                    <span class="parpadea text">TopManage</span>
                </td>
                <td colspan="2">
                    <span class="parpadea text">Examen 1</span>
                </td>
                <td colspan="2">
                    <span class="parpadea text">HTML</span>
                </td>
            </tr>
        </table>
    </div>
    <script>
        var regular_expression = '^(?=(?:.*[A-Z]){1})\S{4,8}$';

        function test(obj) {
            alert(obj.value);
        }
    </script>
</asp:Content>
