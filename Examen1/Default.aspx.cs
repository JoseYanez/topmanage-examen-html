﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Examen1.Models.EE;

namespace Examen1
{
    public partial class _Default : Page
    {
        Usuario users = new Usuario();
        private string pwd_regex = @"^(?=(?:.*\d){1})(?=(?:.*[A-Z])?)(?=(?:.*[a-z])?)\S{4,8}$";
        private string name_regex = @"^([A-Z]{1})([a-z]?)([0-9]?)$";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void txt_age_TextChanged(object sender, EventArgs e)
        {
            int age = 0;
            if (!int.TryParse(txt_age.Text, out age)) {
                txt_age.Text = "";
                lbl_message.Text = "Age Field should be integer!";
                return;
            } else {
                if (age < 0)
                {
                    txt_age.Text = "";
                    lbl_message.Text = "Age Field should be greather than or equal to 0!";
                    return;
                }
                DateTime hoy = DateTime.Today;
                int anios = hoy.Year - age;
                lbl_years.Text = anios.ToString();
            }

        }

        protected void reset_data_Click(object sender, EventArgs e)
        {
            txt_name.Text = "";
            txt_age.Text = "";
            lbl_years.Text = "";
            select_occupation.SelectedIndex = 0;
            txt_pwd.Text = "";
            lbl_message.Text = "";
        }

        protected void commit_data_Click(object sender, EventArgs e)
        {
            string name = "", occupation = "", password = "";
            int age = 0, year = 0;
            Regex name_rex = new Regex(name_regex);
            Regex pass_rex = new Regex(pwd_regex);

            if (txt_name.Text == "")
            //if (!name_rex.IsMatch(txt_name.Text))
                {
                lbl_message.Text = "The Name Field is not valid";
                return;
            }
            
            if (!int.TryParse(txt_age.Text, out age))
            {
                txt_age.Text = "";
                lbl_message.Text = "Age Field should be integer!";
                return;
            } else {
                if (age < 0)
                {
                    txt_age.Text = "";
                    lbl_message.Text = "Age Field should be greather than or equal to 0!";
                    return;
                }
                DateTime hoy = DateTime.Today;
                year = hoy.Year - age;
            }
            if (string.IsNullOrEmpty(txt_pwd.Text) || string.IsNullOrWhiteSpace(txt_pwd.Text))
            {
                lbl_message.Text = "The Password field can not be empty";
                return;
            }
            if (!pass_rex.IsMatch(txt_pwd.Text))
            {
                lbl_message.Text = "The length of the password is greater than or equal to 4 characters and less than or equal to 8 and must contain a number.";
                return;
            }
            if (select_occupation.SelectedItem.Value == "-1")
            {
                lbl_message.Text = "Select valid option in Occupation Field";
                return;
            }
            var user_data = new Usuario();
            name = txt_name.Text;
            password = txt_pwd.Text;
            occupation = select_occupation.SelectedItem.Value;

            user_data.Age = age;
            user_data.Year = year;
            user_data.Name = name;
            user_data.Password = password;
            user_data.Occupation = occupation;

            using(var db = new Model1Container())
            {
                db.UsuarioSet.Add(user_data);
                db.SaveChanges();
            }
            
        }
    }
}